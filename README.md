# apex-docker-compose

This repository contains the docker compose file used to launch the development environment for the Evolve API.

Instructions to start:
1. Make sure you are registered at https://hub.docker.com and have a Docker ID.

2. Install Docker (see https://www.docker.com/products/docker-desktop) and connect it to your Docker ID.

3. Since we will be running multiple containers together, make sure Docker has as many CPUs and as much RAM as you  can afford to share based on your computer's available resources.  You can change the settings in Docker's Advanced settings section.

    ![Docker Advanced Settings Screenshot](.\docker_settings.jpg)

4. Send email to Brian Davis to request access to the Evolve Docker repositories - include your Docker ID in the request.

5. Clone this repository to your desktop.

6. In a command/console window:
    * change directories to the directory where you cloned this repository 
    * execute: `docker-compose up` 

7. The above command should automatically pull and start the containers required for the Evolve API development environment

8. The docker network is bridged with your desktop's network, which means you should be access the Swagger documentation at http://localhost:8010/swagger-ui.html.

9. You should also be able to test the ability to log in using a curl command like this:

    `curl -X POST \`
      `http://localhost:8010/rest/auth/login \`
      `-H 'Accept: application/json' \`
      `-H 'Content-Type: application/json' \`
      `-d '{`
      `"username" : "evadmin@evolvecellular.com",`
      `"password" : "evadmin@evolvecellular.com"`
    `}'`

10. At this point you can start the Apex UI on your Desktop and configure it to use the Evolve API docker container for the backend.

Notes:
* You can stop the containers by doing Ctrl-C in the window where you ran the docker-compose command.
* The first time the containers start up, it creates a new database instance and schema, etc.
* You can stop and start the containers again later to resume development - on subsequent runs the database contents will be retained from previous runs.
* You can use `docker-compose down` to reset everything.
* You can use `docker-compose pull` to make sure you have the latest versions of the docker images.
* The default docker-compose file references the "development" tags.  There is an alternate version of the docker-compose.yml file, named docker-compose-prod.yml that can be used as an example of how to reference the "production" version of the Apex UI containers.  The alternate docker compose file can be used directly, using a command like: `docker-compose -f docker-compose-prod.yml up`

