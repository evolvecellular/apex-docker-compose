# How To Enable Usage Emulation

## Document Update History

| Version | Description                                                  | Date          | Author(s)      |
| ------- | ------------------------------------------------------------ | ------------- | -------------- |
| 0.1     | Draft                                                        | June 4, 2019  | Brian P. Davis |
| 1.0     | Initial Version                                              | June 5, 2019  | Brian P. Davis |
| 1.1     | Remove references to usage_enhancements docker tag (now merged to development) | June 13, 2019 | Brian P. Davis |

## Overview

This document provides instructions on how to control the emulation of summary usage records and, optionally, call detail records in the Docker environment.  These instructions assume you are already familiar with Docker and docker-compose and are familiar with the AME REST API.

There are currently several different ways to generate usage data.  Each method has it advantages and disadvantages, so refer the description below to determine which type of emulation is most suitable for your testing needs.

### Types of Usage Emulation

- **Emulation of summary usage records only**: this type of emulation produces the summarized usage statistics, without generating the underlying individual usage records (CDRs a.k.a. BERs).  This approach populates the database to provide data for the /rest/enterprises/{enterpriseId}/accounts/{accountId}/usage APIs, which are used to populate the usage charts on the AME web portal.  This approach does not populate the database with CDRs, so the /rest/enterprises/{enterpriseId}/accounts/{accountId}/cdr APIs will return no associated records.  This means that the CDR download links in the AME web portal will return empty files.  The advantage of this approach of emulation is that it bypasses the billing system entirely, simplifying the emulation approach and providing records immediately for testing purposes.
- **Emulation of call detail records (CDRs a.k.a. BERs) and resulting usage summary data**: this type of emulation sends a request to the billing system (evolve-ber-publisher container) to emulate some number of individual usage records.  Through a sequence of steps, similar to those used in production, those records are then "published" and summarized, populating the database with the individual records as well as the summarized usage records.  This tests the summarization logic within the billing system and is a more complete type of emulation from the system perspective.  This approach populates the database to support both "usage" and "cdr" endpoints.  The disadvantage of this approach is that it takes time for the request to be fulfilled, as the records are processed and published and finally summarized.  For certain types of testing (like automated testing), this delay may not be acceptable.

### Getting Started

#### Set up

To get started, first make sure you have a laptop or lab environment you can use as a Docker host.  Then, customize the docker compose file based on your testing needs.  The most up-to-date template of the docker-compose.yml file can always be found at: <https://bitbucket.org/evolvecellular/apex-docker-compose/src/master/docker-compose.yml>.  The Bitbucket version is useful as a template for your docker environment - you will want to make changes depending on the type of testing you plan to perform.  The example below shows a modified version, suitable for testing on-demand usage generation.

*SAMPLE_ACCOUNT_USAGE_EMULATION_ENABLED* - look for this environment variable, which is defined for the evolve-apex-api-platform container.  This environment variable may be set to either `"true"` or `"false"`.

- `"true"` - tells the evolve-apex-api-platform to automatically (once a day) generate summary usage records (only) for the previous day specifically for the "Sample Enterprise"/"Sample Account One" account.  The usage characteristics are randomized (number of total calls, number of mobile calls, number of deflections, etc.).
- `"false"` - disables the automatic generation of usage summary records for the Sample Account.

*USAGE_EMULATION_ENABLED* - look for this environment variable, which is defined for the evolve-ber-publisher.  This environment variable may be set to either `"true"` or `"false"`.

- `"true"` - tells the evolve-ber-publisher to automatically (once an hour) generate call detail records for the previous hour.  The usage characteristics are randomized (number of total calls, number of mobile calls, number of deflections, etc.).  CDRs will be generated for all active ("In Service") phone numbers in the Avaya enterprise (any Account under the Avaya Enterprise).
- `"false"` - disables the automatic generation of emulated CDRs.  On-demand emulation is still possible through the use of a special purpose API (described further in this document).

Advice: set USAGE_EMULATION_ENABLED to true if you plan to leave your Docker environment running for hours and/or days at a time, and you want to see some usage data but you don't really care about the characteristics of the data.  Set USAGE_EMULATION_ENABLED to false if you plan to use the API to control the emulation of usage data, to test specific scenarios and use-cases related to usage data.  This will avoid confusion between which usage data was automatically generated and which data was manually requested.

Finally, if you plan to use the API to request usage data emulation, make sure you have [Postman](https://www.getpostman.com/) or curl or some similar tool readily accessible and you are familiar with how to use that tool.

```yaml
version: '3.2'
services:
    evolve-cassandra:
      container_name: evolve-cassandra
      image: 'evolvecellular/evolve-cassandra-apex:latest'
      hostname: evolve-cassandra
      expose:
        - "9042"
        - "7199"
        - "9160"
      volumes:
        - /var/lib/cassandra
      networks:
        evolve-network:
          ipv4_address: 192.168.100.100
    evolve-rabbitmq:
      container_name: evolve-rabbitmq
      image: 'rabbitmq:3.7.7-management'
      hostname: evolve-rabbitmq
      expose:
        - "5672"
        - "15672"
      networks:
        evolve-network:
          ipv4_address: 192.168.100.101
    evolve-apex-api-platform:
      container_name: evolve-apex-api-platform
      image: 'evolvecellular/evolve-apex-api-platform:development'
      hostname: evolve-apex-api-platform
      ports:
        - "8010:8010" # Evolve API
      depends_on:
        - evolve-rabbitmq
        - evolve-cassandra
      environment:
        CASSANDRA_HOST: 192.168.100.100
        CASSANDRA_PORT: 9042
        RABBIT_HOST: 192.168.100.101
        STARTUP_DELAY: 90s
        SAMPLE_ACCOUNT_USAGE_EMULATION_ENABLED: "false"
      networks:
        evolve-network:
          ipv4_address: 192.168.100.102
    evolve-call-routing-emulator:
      container_name: evolve-callrouting-emulator
      image: 'evolvecellular/evolve-callrouting-emulator:development'
      hostname: evolve-callrouting-emulator
      depends_on:
        - evolve-rabbitmq
        - evolve-apex-api-platform
      environment:
        RABBIT_HOST: 192.168.100.101
        STARTUP_DELAY: 20s
      networks:
        evolve-network:
          ipv4_address: 192.168.100.103
    evolve-frontdoor-server:
      container_name: evolve-frontdoor-server
      image: 'evolvecellular/evolve-frontdoor-server:development'
      hostname: evolve-frontdoor-server
      depends_on:
        - evolve-rabbitmq
        - evolve-apex-api-platform
      environment:
        RABBIT_HOST: 192.168.100.101
        STARTUP_DELAY: 20s
        ACTIVE_PROFILE: emulation
        FDAPI_HOST: localhost
        FDAPI_PORT: 8080
      networks:
        evolve-network:
          ipv4_address: 192.168.100.104
    evolve-ber-publisher:
      container_name: evolve-ber-publisher
      image: 'evolvecellular/evolve-ber-publisher:development'
      hostname: evolve-ber-publisher
      depends_on:
        - evolve-rabbitmq
        - evolve-cassandra
        - evolve-apex-api-platform
      environment:
        CASSANDRA_HOST: 192.168.100.100
        CASSANDRA_PORT: 9042
        RABBIT_HOST: 192.168.100.101
        STARTUP_DELAY: 60s
        USAGE_EMULATION_ENABLED: "false"
      networks:
        evolve-network:
          ipv4_address: 192.168.100.105
networks:
  evolve-network:
    driver: bridge
    ipam:
      driver: default
      config:
        - subnet: 192.168.0.0/16
```
<div style="text-align: center;">
    <span style="font-style:italic;">
        Example docker-compose.yml file
    </span>
</div>
#### Start up

From a windows command window, change directory to where your docker-compose.yml file is store, and then execute the following commands:

1. `docker-compose pull` - to make sure you have the latest version of all of the containers
2. `docker-compose down` - to make sure you are starting with a clean environment
3. `docker-compose up` - to start everything up.  It will take several minutes to completely initialize the system, depending on the speed of your Docker host

Now, the AME API server is listening on port 8010 of your Docker host.  You can direct API requests at this port or, most likely, configure a running AME UI instance to communicate with the API server.

#### Create Call Flow and active Phone number in Avaya Test account

To make it possible to generate emulated call detail records, we will need to create at least one callflow and one active number in an Avaya account.  The simplest/quickest thing to do is to use the AME UI to log into the Avaya "Test" account.  Then create a simple by-pass callflow, publish it, and then assign one of the numbers to it.

You can create any number of callflows and assign multiple numbers to the same callflow, or different callflows, etc.

#### Next Steps

At this point, if you have USAGE_EMULATION_ENABLED set to true, you should see usage data generated within the Avaya Test account (or any Avaya account that contains active numbers), within approximately 30 minutes of starting the Docker environment and creating the In Service number(s).  From there you should see additional data generated each hour for as long as the Docker system is running.

If you have USAGE_EMULATION_ENABLED set to false, refer to the next section of this document to learn how to prompt the generation of call detail records.

### How to use the Usage Emulation API

There is an API endpoint within the API server that is only active in development/test environments.  This API endpoint can be used to submit a usage data emulation request - a request to have the system emulate a specific amount of usage-related records.

`POST /rest/enterprises/{enterpriseId}/accounts/{accountId}/usageEmulation`

Body:

```json
{
    "usageHour" : "2019-05-22T14:00:00.00Z",
	"usageEmulationDetails" : [
		{
			"calledNumber" : "+18005551212",
			"numCalls" : 1000,
			"numLandlineCalls" : 200,
			"numMobileCalls" : 800,
			"numDeflectionsOffered" : 800,
              "numDeflections" : 750,
              "segmentTag" : "en1",
              "numVoiceSeconds" : 2000,
              "numMobileSeconds" : 1000
		}
	]
}
```

The {enterpriseId} and {accountId} in the API call indicate which account will contain the emulated usage data.  The body of the request contains the specifics of each request.  There will need to be one request per usage hour,  so you may want to use Postman to store a collection of requests for the different hours of the day, or use a script or some other tool to generate requests across a whole day or across multiple days.  Note: a "usageHour" is a timestamp string in ISO 8601 format.  It is typically set to the beginning of the hour, and indicates the hour for which the usage data will be generated.

For each usage hour, you then add a list of entries, one per phone number in the account (as desired), specifying how many calls should be emulated, how many should be mobile/landline, how many should be deflected, etc.  They could be multiple entries per phone number, in case you want to specify different segment tags, for example.

**Note:** the emulation logic does not currently emulate "front door event" data, so it does not currently support "segmentTag" and "numDeflectionsOffered" parameters.  This parameters are exposed in this API for future use.

The API user is only required to supply the "calledNumber" and "numCalls" parameters for each entry.  Any parameters that are not supplied by the user will be assigned generated, randomized default values.

Here is an example API call:

```
curl -X POST \
  http://localhost:8010/rest/enterprises/8282de77-51d4-43fd-891d-64a793032aa4/accounts/6bf3d80c-9ebd-4996-a9d1-44a7e85ef232/usageEmulation \
  -H 'Accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJldmFkbWluQGV2b2x2ZWNlbGx1bGFyLmNvbSIsImV4cCI6MTU1OTc0NzE5NX0.varlorQlgeVmHLlbiBWnH-W1Qbgd4x5IpNX0Ri7ZWnM' \
  -H 'Content-Type: application/json' \
  -d '{
	"usageHour" : "2019-06-04T14:00:00.00Z",
	"usageEmulationDetails" : [
		{
			"calledNumber" : "+18005551212",
			"numCalls" : 1000,
			"numLandlineCalls" : 200,
			"numMobileCalls" : 800,
			"numDeflectionsOffered" : 800,
              "numDeflections" : 750
		}
	]
}'
```

Prior to invoking the above example API request, the 8005551212 number as added to the Avaya Test account (6bf3d80c-9ebd-4996-a9d1-44a7e85ef232) and assigned to a published callflow so that it is In Service.  This request will generate 1000 calls for that number, for the 2pm (GMT) hour, with most of them being mobile and deflected.

Since the BER records are published (in the Docker environment) at the 5 minute and 35 minute mark of every hour, you will need to wait up to 30 minutes to see the usage records reflected in the appropriate APIs.

After waiting, this API can be used to fetch the usage records:

```
curl -X GET \
  'http://localhost:8010/rest/enterprises/8282de77-51d4-43fd-891d-64a793032aa4/accounts/6bf3d80c-9ebd-4996-a9d1-44a7e85ef232/usage/findAll?timeFrame=currentDay' \
  -H 'Accept: application/json' \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJldmFkbWluQGV2b2x2ZWNlbGx1bGFyLmNvbSIsImV4cCI6MTU1OTc2MjAyN30.i5i5rnvBEKRZtvKl5X80XoWoHOyLAhOUrPF81IA7ylw' \
  -H 'Content-Type: application/json'
```

with this example response:

```json
{
    "accountId": "6bf3d80c-9ebd-4996-a9d1-44a7e85ef232",
    "startHour": "2019-06-04T10:00:00-04:00",
    "endHour": "2019-06-04T10:00:00-04:00",
    "timeFrame": "currentDay",
    "aggregationLevel": "day",
    "sort": [
        "usageHour,asc"
    ],
    "numCalls": 1000,
    "numLandlineCalls": 200,
    "numMobileCalls": 800,
    "numOtherCalls": 0,
    "numVoiceMinutes": 3143,
    "numMobileMinutes": 1537,
    "numDeflections": 750,
    "numDeflectionsOffered": 0,
    "lastUsageUpdateDate": "2019-06-04T15:35:34.736-04:00",
    "usageRecords": [
        {
            "accountId": "6bf3d80c-9ebd-4996-a9d1-44a7e85ef232",
            "phoneNumberId": "cec6db10-248a-4cac-b9f5-dac883dc5caa",
            "phoneNumber": "+18005551212",
            "usageHour": "2019-06-04T00:00:00-04:00",
            "segmentTag": "-",
            "numCalls": 1000,
            "numLandlineCalls": 200,
            "numMobileCalls": 800,
            "numOtherCalls": 0,
            "numVoiceMinutes": 3143,
            "numMobileMinutes": 1537,
            "numDeflections": 750,
            "numDeflectionsOffered": 0
        }
    ]
}
```

